# -*- coding: utf-8 -*-
from decimal import Decimal
from shop.util.address import get_shipping_address_from_request
from shop.models import OrderItem
from django.conf import settings
from django.conf.urls import patterns, url
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
import logging
from shop.util.decorators import on_method, shop_login_required, order_required

class StoreRateShipping(object):
    url_namespace = 'store'
    backend_name = 'EnTienda'
    backend_verbose_name = _('Entrega en tienda')

    def __init__(self, shop):
        self.logger = logging.getLogger('transaction')
        self.shop = shop  # This is the shop reference, it allows this backend
        # to interact with it in a tidy way (look ma', no imports!)

        self.rate = 0

    @on_method(shop_login_required)
    @on_method(order_required)
    def view_process_order(self, request):
        """
        A simple (not class-based) view to process an order.

        This will be called by the selection view (from the template) to do the
        actual processing of the order (the previous view displayed a summary).

        It calls shop.finished() to go to the next step in the checkout
        process.
        """

        order = self.shop.get_order(request)
        self.shop.add_shipping_costs(self.shop.get_order(request),
                                     'Costo de envio',
                                     Decimal(self.rate))
        self.logger.info('Shipping costs: {0} | rate: {1} | order: {2}'.format(order.shipping_costs, self.rate, order.id))
        if order.shipping_costs == self.rate:
            return self.shop.finished(self.shop.get_order(request))
        else:
            return redirect('checkout_selection')
        # That's an HttpResponseRedirect

    @on_method(shop_login_required)
    @on_method(order_required)
    def view_display_fees(self, request):
        """
        A simple, normal view that displays a template showing how much the
        shipping will be (it's an example, alright)
        """

        order = self.shop.get_order(request)
        total = self.shop.get_order_total(order)
        costo = None
        mensaje = '** Deberá retirar su producto en la Tienda Principal: ' \
                      '11 Calle, 10 Ave. Bo. Los Andes, puedes contactarnos al 2544-0124, te esperamos!'

        self.rate = Decimal(0)
        self.logger.info('Order: {0} | Shipping: {1}'.format(order.id, self.rate, ))
        ctx = {}
        ctx.update({'shipping_costs': self.rate, 'mensaje': mensaje, })
        return render_to_response('shop/shipping/store_rate/display_fees.html',
            ctx, context_instance=RequestContext(request))

    def get_urls(self):
        """
        Return the list of URLs defined here.
        """
        urlpatterns = patterns('',
            #url(r'^$', self.view_display_fees, name='store'),
            url(r'^$', self.view_process_order, name='store'),
            url(r'^process/$', self.view_process_order, name='store_process'),
        )
        return urlpatterns
