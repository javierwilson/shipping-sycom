# -*- coding: utf-8 -*-
from decimal import Decimal
from shop.util.address import get_shipping_address_from_request
from shop.models import OrderItem
from django.conf import settings
from django.conf.urls import patterns, url
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
import logging
from shop.util.decorators import on_method, shop_login_required, order_required
from website.models import Tarifa

class BusRateShipping(object):
    url_namespace = 'bus'
    backend_name = 'Envio'
    backend_verbose_name = _('Envio')

    def __init__(self, shop):
        self.logger = logging.getLogger('transaction')
        self.shop = shop  # This is the shop reference, it allows this backend
        # to interact with it in a tidy way (look ma', no imports!)

        self.rate = 0

    @on_method(shop_login_required)
    @on_method(order_required)
    def view_process_order(self, request):
        """
        A simple (not class-based) view to process an order.

        This will be called by the selection view (from the template) to do the
        actual processing of the order (the previous view displayed a summary).

        It calls shop.finished() to go to the next step in the checkout
        process.
        """

        order = self.shop.get_order(request)
        self.shop.add_shipping_costs(self.shop.get_order(request),
                                     'Costo de envio',
                                     Decimal(self.rate))
        print 'Shipping costs: {0} | rate: {1} | order: {2}'.format(order.shipping_costs, self.rate, order.id)
        self.logger.info('Shipping costs: {0} | rate: {1} | order: {2}'.format(order.shipping_costs, self.rate, order.id))
        if order.shipping_costs == self.rate:
            return self.shop.finished(self.shop.get_order(request))
        else:
            return redirect('checkout_selection')
        # That's an HttpResponseRedirect

    @on_method(shop_login_required)
    @on_method(order_required)
    def view_display_fees(self, request):
        """
        A simple, normal view that displays a template showing how much the
        shipping will be (it's an example, alright)
        """

        order = self.shop.get_order(request)
        total = self.shop.get_order_total(order)
        total_weight = Decimal(0)
        mensaje = ''
        for oi in OrderItem.objects.filter(order=order):
            total_weight += oi.product.peso * oi.quantity
        shipping_address = get_shipping_address_from_request(request)
        municipio = shipping_address.municipio
        tarifas = Tarifa.objects.filter(destino=municipio.zona, peso_desde__lte=total_weight, peso_hasta__gte=total_weight)
        if len(tarifas) > 0 :
            costo = tarifas[0].precio
        else:
            costo = None
            #mensaje = '** Sus compras son menores a Lps. 500, deberá retirar su producto en la Tienda Principal: '
            mensaje = '** No se encontró una tarifa, deberá retirar su producto en la Tienda Principal: ' \
                      'Altamira de este, frente a donde fue la Vicky. Managua, Nicaragua.'

        #if municipio.id == 352 and total > 500:
        #    costo = Decimal(0)
        #    mensaje = '** Su envio es gratis'

        if costo:
            self.rate = costo
        else:
            self.rate = Decimal(0)
        self.logger.info('Order: {0} | Shipping: {1} | Tarifa: {2} | Peso: {3} | Zona: {4}'.format(order.id, self.rate, tarifas, total_weight, municipio.zona))
        ctx = {}
        ctx.update({'shipping_costs': self.rate, 'mensaje': mensaje, 'municipio':municipio })
        #return render_to_response('shop/shipping/bus_rate/display_fees.html',
        #    ctx, context_instance=RequestContext(request))
        return redirect('bus_process')

    def get_urls(self):
        """
        Return the list of URLs defined here.
        """
        urlpatterns = patterns('',
            url(r'^$', self.view_display_fees, name='bus'),
            #url(r'^$', self.view_process_order, name='bus'),
            url(r'^process/$', self.view_process_order, name='bus_process'),
        )
        return urlpatterns
